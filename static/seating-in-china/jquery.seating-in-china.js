;(function($, window, document,undefined) {
    //定义Seating的构造函数
    var Seating = function(ele, opt) {
        this.$element = ele,
            this.defaults = {
                'rows': 20,
                'cols': 15
            },
            this.options = $.extend({}, this.defaults, opt)
    }
    //定义Beautifier的方法
    Seating.prototype = {
        //初始化会议画布
        initMeetingPlace: function() {
            //计算画布的宽度
            this.$element.width(this.options.rows*30 +3);
            this.$element.height(this.options.cols*30 +3);
            this.$element.html("");
            for(var c=1;c<=this.options.cols;c++){
                for(var r=1;r<=this.options.rows;r++){
                    var divHtml = "<div class='seat' data-id='"+c+"-"+r+"'>&nbsp;</div>";
                    this.$element.append(divHtml);
                }
            }

            return this.$element;
        },
        initSeatSelection:function (dataArr) {

            for(var i=0;i<dataArr.length;i++){
                $("[data-id='"+dataArr[i]+"']").addClass("active");
            }
        }
    }
    //在插件中使用Beautifier对象
    $.fn.seating = function(options) {
        //创建Seating的实体
        var seating = new Seating(this, options);
        //调用其方法
        return seating.initMeetingPlace();
    }
    $.fn.initSeatSelection = function (dataArr) {

        for(var i=0;i<dataArr.length;i++){
            $("[data-id='"+dataArr[i]+"']").addClass("active");
        }
    }

    $.fn.sortSeatByParam = function (params,selectedData,reSeat) {

            //从左至右
            var compareL2R = function (obj1, obj2) {
                var row1 = parseInt(obj1.split("-")[0]);
                var col1 = parseInt(obj1.split("-")[1]);

                var row2 = parseInt(obj2.split("-")[0]);
                var col2 = parseInt(obj2.split("-")[1]);

                if (row1 < row2) {
                    return -1;
                } else if (row1 > row2) {
                    return 1;
                } else {
                    //相等的话就比col
                    if(col1 < col2){
                        return -1;
                    }else if(col1 > col2){
                        return 1;
                    }else{
                        return 0;
                    }
                }
            }


            var compareR2L = function (obj1, obj2) {
                var row1 = parseInt(obj1.split("-")[0]);
                var col1 = parseInt(obj1.split("-")[1]);

                var row2 = parseInt(obj2.split("-")[0]);
                var col2 = parseInt(obj2.split("-")[1]);

                if (row1 < row2) {
                    return -1;
                } else if (row1 > row2) {
                    return 1;
                } else {
                    //相等的话就比col
                    if(col1 < col2){
                        return 1;
                    }else if(col1 > col2){
                        return -1;
                    }else{
                        return 0;
                    }
                }
            }

            var compareT2B = function (obj1, obj2) {
                var row1 = parseInt(obj1.split("-")[0]);
                var col1 = parseInt(obj1.split("-")[1]);

                var row2 = parseInt(obj2.split("-")[0]);
                var col2 = parseInt(obj2.split("-")[1]);

                if (col1 < col2) {
                    return -1;
                } else if (col1 > col2) {
                    return 1;
                } else {
                    //相等的话就比col
                    if(row1 < row2){
                        return -1;
                    }else if(row1 > row2){
                        return 1;
                    }else{
                        return 0;
                    }
                }
            }

            var compareB2T = function (obj1, obj2) {
                var row1 = parseInt(obj1.split("-")[0]);
                var col1 = parseInt(obj1.split("-")[1]);

                var row2 = parseInt(obj2.split("-")[0]);
                var col2 = parseInt(obj2.split("-")[1]);

                if (col1 < col2) {
                    return -1;
                } else if (col1 > col2) {
                    return 1;
                } else {
                    //相等的话就比col
                    if(row1 < row2){
                        return 1;
                    }else if(row1 > row2){
                        return -1;
                    }else{
                        return 0;
                    }
                }
            }
            if(params.rule=="L2R"){
                selectedData.sort(compareL2R);
            }else if(params.rule=="R2L"){
                selectedData.sort(compareR2L);
            }else if(params.rule=="T2B"){
                selectedData.sort(compareT2B);
            }else if(params.rule=="B2T"){
                selectedData.sort(compareB2T);
            }
        if(reSeat){
            var index = 1;
            for(var i=0;i<selectedData.length;i++){
                $("[data-id='"+selectedData[i]+"']").html(index);
                $("[data-id='"+selectedData[i]+"']").attr("order-index",index);
                index ++;
            }

            if(params.long){
                if(params.rule=='R2L'||params.rule=='L2R'){
                    var firstRow = selectedData[0].split("-")[0];
                    firstRow++;
                    var nextRows = new Array();
                    var dataArr = new Array();
                    for(var i=0;i<selectedData.length;i++){
                        var row = selectedData[i].split("-")[0];
                        if(row%firstRow==0){
                            nextRows.push(selectedData[i]);
                            var cc =$("[data-id='"+selectedData[i]+"']").html();
                            dataArr.push(cc);
                        }
                    }
                    if(params.rule=='L2R'){
                        nextRows.sort(compareR2L);
                        for(var j=0;j<nextRows.length;j++){
                            $("[data-id='"+nextRows[j]+"']").html(dataArr[j]);
                            $("[data-id='"+selectedData[i]+"']").attr("order-index",index);
                        }
                    }
                    if(params.rule=='R2L'){
                        nextRows.sort(compareL2R);
                        for(var j=0;j<nextRows.length;j++){
                            $("[data-id='"+nextRows[j]+"']").html(dataArr[j]);
                            $("[data-id='"+selectedData[i]+"']").attr("order-index",index);
                        }
                    }
                }
                if(params.rule=='T2B'||params.rule=='B2T'){
                    var firstCol = selectedData[0].split("-")[1];
                    firstCol++;
                    var nextCols = new Array();
                    var dataArr = new Array();
                    for(var i=0;i<selectedData.length;i++){
                        var col = selectedData[i].split("-")[1];
                        if(col%firstCol==0){
                            nextCols.push(selectedData[i]);
                            var cc =$("[data-id='"+selectedData[i]+"']").html();
                            dataArr.push(cc);
                        }
                    }
                    if(params.rule=='T2B'){
                        nextCols.sort(compareB2T);
                        for(var j=0;j<nextCols.length;j++){
                            $("[data-id='"+nextCols[j]+"']").html(dataArr[j]);
                            $("[data-id='"+selectedData[i]+"']").attr("order-index",index);
                        }
                    }
                    if(params.rule=='B2T'){
                        nextCols.sort(compareT2B);
                        for(var j=0;j<nextCols.length;j++){
                            $("[data-id='"+nextCols[j]+"']").html(dataArr[j]);
                            $("[data-id='"+selectedData[i]+"']").attr("order-index",index);
                        }
                    }
                }
            }
        }


        return selectedData;
    }
})(jQuery, window, document);